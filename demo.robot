*** Settings ***
Documentation               This is a demo for RbeeSolar
Library                     Selenium2Library
Suite Setup                 Start Browser     ${URL}
Suite Teardown              Close Browser
      
      
*** Variables ***
${URL}                 https://work.pvmeter.com/?locale=en_GB
${login}               maurice@rbee.fr
${installer_login}     demo
${good_pass}           123456
${installer_pass}      demo69550
${bad_pass}            xdxd32       


*** Keywords ***
Start_Browser
    [Arguments]    ${URL}
    Open Browser    ${URL}     browser=headlesschrome
	Set Selenium Implicit Wait    10
      
Close Browser
    Close All Browsers
   
Log In
    [Arguments]    ${account}     ${pass}
	Wait Until Page Contains Element    xpath://div[@id="loginBox"]
    Input Text                          xpath://input[@name="j_username"]    ${account}
    Input Text                          xpath://input[@name="j_password"]    ${pass}
    Click Element                       xpath://table[@id="loginFields"]//input[@type="submit"]

Log Out
    Click Element                       xpath://div[@id="gwt-debug-mainLayout"]//a[2]
	Handle Alert

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute JavaScript    window.scrollTo(${x_location},${y_location})
      
*** Test Cases ***
TC1:Login with proper credentials
    [Documentation]     Login into website   
    [Tags]    Login  Producer
    Go to                   ${URL}
    Log In                  ${login}    ${good_pass} 
    Wait Until Page Contains Element    xpath://div[@id="gwt-debug-perspectivesLayout"]    10
    Page Should Contain Element         xpath://div[@id="gwt-debug-mainLayout"]
	Log Out


TC2:Login using invalid credentials
   [Documentation]     Login into website using wrong password
   [Tags]    Login  Negative
   Go to                   ${URL}
   Log In                  ${login}    ${bad_pass}}
   Page Should Contain Element         id:error

TC3:Password Lost
   [Documentation]    Verify error message when user entered wrong image code
   [Tags]     Login  Negative
   Go to                   ${URL}
   Click Element                       xpath://a[@href='?captcha']
   Input Text                          xpath://input[@name='login']  ${login}
   Input Text                          xpath://input[@name='captcha_code']  ${bad_pass}
   Click Element                       xpath://input[@value='Send']
   Page Should Contain Element         id:error
   
TC4:Search customer by name
    [Documentation]     Search for a customer  
    [Tags]  Producer
    Go to                   ${URL}
    Log In                  ${installer_login}    ${installer_pass} 
    Wait Until Page Contains Element    xpath://div[@id="gwt-debug-perspectivesLayout"]    20
    Page Should Contain Element         xpath://div[@id="gwt-debug-mainLayout"]
	Page Should Contain Element         xpath://div[@id="gwt-debug-installerSolarPerspective"]//div[1]/table//tr[2]/td[2]/div
	Input Text                          xpath://div[@id="gwt-debug-installerSolarPerspective"]//tbody//div/div[1]/div[1]//input[@class="gwt-TextBox"]    LAGIER
    Click Element                       xpath://td[3]/button
	Wait Until Page Contains Element    xpath://div[@id='gwt-debug-installerSolarPerspective']/div/div/div/table[1]/tbody/tr[1]/td[2]/div[contains(text(),"LAGIER")]
    Log Out	

TC5:Edit selected customer
    [Documentation]     Open edit view for selected customer   
    [Tags]  Producer  Edit
    Go to                   ${URL}
    Log In                  ${installer_login}    ${installer_pass} 
    Wait Until Page Contains Element    xpath://div[@id="gwt-debug-perspectivesLayout"]    20
    Page Should Contain Element         xpath://div[@id="gwt-debug-mainLayout"]
	Page Should Contain Element         xpath://div[@id="gwt-debug-installerSolarPerspective"]//div[1]/table//tr[2]/td[2]/div
	Input Text                          xpath://div[@id="gwt-debug-installerSolarPerspective"]//tbody//div/div[1]/div[1]//input[@class="gwt-TextBox"]    LAGIER
    Click Element                       xpath://td[3]/button
	Wait Until Page Contains Element    xpath://div[@id="gwt-debug-installerSolarPerspective"]/div/div/div/table[1]/tbody/tr[1]/td[2]/div[contains(text(),"LAGIER")]
	Click Element                       xpath://div[@id="gwt-debug-installerSolarPerspective"]//td[6]/div/a
	Wait Until Page Contains Element    xpath://a[contains(@class, "btn btn-primary")]
	Page Should Contain Element         xpath://div[@id="gwt-debug-installerSolarPerspective"]//table[4]//td[2]/div[contains(text(),"�diter une installation")]
    Page Should Contain Element         xpath://div[@id="gwt-debug-solarInstallationFormView"]/div/div[1]/table[3]/tbody
	Scroll Page To Location    0    0
	Sleep      5
	Log Out	

	
TC6:Language change
    [Documentation]     Changing language on producer website 
    [Tags]  Producer
    Go to                   ${URL}
    Log In                  ${login}    ${good_pass} 
    Wait Until Page Contains Element    xpath://div[@id="gwt-debug-perspectivesLayout"]    20
    Page Should Contain Element         xpath://div[@id="gwt-debug-mainLayout"]
    Click Element                       xpath://div[@id="gwt-debug-LanguageChooserWidgetMainLayout"]
	Click Element                       xpath://div[@id="gwt-debug-LanguageChooserList"]//table[2]//div[@class="gwt-Label"]
	Wait Until Page Contains            Dashboard     20
	Click Element                       xpath://div[@id="gwt-debug-LanguageChooserWidgetMainLayout"]
	Click Element                       xpath://div[@id="gwt-debug-LanguageChooserList"]//table[1]//div[@class="gwt-Label"]
	Wait Until Page Contains            Tableau de bord     20
	Log Out
   

